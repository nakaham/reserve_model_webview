package com.example.nakaham.reserve_model_webview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.example.nakaham.reserve_model_webview.api.apiParam;
import com.example.nakaham.reserve_model_webview.api.apiReturnCallbackFunction;
import com.example.nakaham.reserve_model_webview.api.hooker.BayTicket.TickePia.ReturnInterface.iReturnOnBayTicket;
import com.example.nakaham.reserve_model_webview.api.hooker.BayTicket.TickePia.apiInterfaceBayTicket;
import com.example.nakaham.reserve_model_webview.api.hooker.BayTicket.TickePia.apiParamBayTicket;
import com.example.nakaham.reserve_model_webview.api.hooker.BayTicket.TickePia.ticketData;
import com.example.nakaham.reserve_model_webview.api.hooker.TickePia.ReturnInterface.iReturnOnTicketPia;
import com.example.nakaham.reserve_model_webview.api.hooker.TickePia.apiInterfaceTicketPia;
import com.example.nakaham.reserve_model_webview.api.hooker.TickePia.apiParamTicketPia;

import java.util.List;

public class MainActivity extends AppCompatActivity implements apiReturnCallbackFunction, iReturnOnTicketPia, iReturnOnBayTicket {
    // ちけっとぴあ
    private apiInterfaceTicketPia api;
    // ベイチケ
    //private apiInterfaceBayTicket api;
    private WebView myWebView;

    private int retryCnt = 0;
    // 時間計測用
    private long startTime;
    private long stopTime;

    private void initProcess(){
        retryCnt = 0;
        seqNoScreen1 = 0;
        seqNoScreen2 = 0;
        seqNoExitSeq = 0;

    }

    /* スクリーン1の検索ワード、URL（チケットぴあ） */
    private String s1Word = "内野指定席Ｃ";
    //private String s1URL = "http://t.pia.jp/sports/baseball/rls/rls_select.jsp?artistCd=11026613&date=20171021";
    private String s1URL = "http://t.pia.jp/pia/ticketInformation.do?eventCd=1820544&perfCd=001&rlsCd=001";

    /* スクリーン1の検索ワード、URL（ベイチケ） */
    //private String s1Word = "ホーム外野立ち見ライト";
    //private String s1URL = "https://ticket.baystars.co.jp/pc/sales/ticket/201805241745";

    /* スクリーン2の検索ワード */
    private String s2Word = "駐車券";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // 計測開始
        startTime = System.currentTimeMillis();

        initProcess();
        // ちけっとぴあ
        api = new apiInterfaceTicketPia(this);
        // ベイチケ
        //api = new apiInterfaceBayTicket(this);
        setContentView(R.layout.activity_main);
        myWebView = (WebView)findViewById(R.id.webView1);
        seqNoScreen1 = api.searchAndGetLinkTicketPiaBaseBall(s1Word, s1URL, myWebView, apiParamTicketPia.pageType.baseballPage, this);
        //seqNoScreen1 = api.searchAndGetLinkBayTicket(s1Word, s1URL, myWebView, apiParamBayTicket.pageType.baseballPage, this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
    // シーケンス番号
    private int seqNoScreen1 = 0;
    private int seqNoScreen2 = 0;
    private int seqNoExitSeq = 0;

    @Override
    public void onCompleteSearchAndGetLinkTicketPiaBaseball(apiParamTicketPia param, String url) {
        // とりあえず今回はモデルケーステストなので、これ検索すれば・・・というキーワードは分かっているので
        // それで検索した時の時間を計る
        if (param.seqNo == seqNoScreen1) {
            Log.i("CompStartUp", "url:" + url);
            Toast.makeText(this, "スクリーン1成功", Toast.LENGTH_SHORT).show();
            myWebView.setWebViewClient(new WebViewClient(){
                @Override
                public void onPageFinished(WebView view, String url) {
                    // WebViewの表示更新
                    myWebView.getSettings().setLoadsImagesAutomatically(true);
                    myWebView.getSettings().setJavaScriptEnabled(true);
                    myWebView.setVisibility(View.VISIBLE);

                    // 計測終了
                    stopTime = System.currentTimeMillis();
                    long time = stopTime - startTime;
                    int second = (int) (time/1000);
                    int comma = (int) (time%1000);
                    Toast.makeText(MainActivity.this, "終了:" + ((second + "秒" + comma).toString()), Toast.LENGTH_SHORT).show();
                    Log.i("ProcessComp", "終了:" + ((second + "秒" + comma).toString()));
                }});
            // WebViewの表示更新
            myWebView.getSettings().setLoadsImagesAutomatically(true);
            myWebView.getSettings().setJavaScriptEnabled(true);
            myWebView.setVisibility(View.VISIBLE);

            // 計測終了
            stopTime = System.currentTimeMillis();
            long time = stopTime - startTime;
            int second = (int) (time/1000);
            int comma = (int) (time%1000);
            Toast.makeText(MainActivity.this, "終了:" + ((second + "秒" + comma).toString()), Toast.LENGTH_SHORT).show();
            Log.i("ProcessComp", "終了:" + ((second + "秒" + comma).toString()));

            myWebView.loadUrl(url);
        } else {
            Toast.makeText(this, "失敗('A`)", Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    public void onCompleteSearchAndGetLinkBayTicket(apiParamBayTicket param, List<ticketData> list) {
        // とりあえず今回はモデルケーステストなので、これ検索すれば・・・というキーワードは分かっているので
        // それで検索した時の時間を計る
        if (param.seqNo == seqNoScreen1) {
            Toast.makeText(this, "スクリーン1成功", Toast.LENGTH_SHORT).show();
            myWebView.setWebViewClient(new WebViewClient(){
                @Override
                public void onPageFinished(WebView view, String url) {
                    // WebViewの表示更新
                    myWebView.getSettings().setLoadsImagesAutomatically(true);
                    myWebView.getSettings().setJavaScriptEnabled(true);
                    myWebView.setVisibility(View.VISIBLE);

                    // 計測終了
                    stopTime = System.currentTimeMillis();
                    long time = stopTime - startTime;
                    int second = (int) (time/1000);
                    int comma = (int) (time%1000);
                    Toast.makeText(MainActivity.this, "終了:" + ((second + "秒" + comma).toString()), Toast.LENGTH_SHORT).show();
                    Log.i("ProcessComp", "終了:" + ((second + "秒" + comma).toString()));
                }});
        } else {
            Toast.makeText(this, "失敗('A`)", Toast.LENGTH_SHORT).show();
        }

    }
}
