package com.example.nakaham.reserve_model_webview;

import android.app.Service;
import android.content.Intent;
import android.content.res.AssetManager;
import android.os.Debug;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;


// 参考→https://android.g.hatena.ne.jp/keigoi/20100211/1265892976
// addjavascriptInterface の JS ⇔ java 間データ型変換
// http://zentoo.hatenablog.com/entry/20120507/1336399651

public class MyService extends Service {

    String title; // JavaScriptからのコールバックで設定される
    long start; // onStartが呼ばれた時刻
    long fetch; // loadUrlを呼んだ時刻
    WebView myWebView;

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public void onDestroy() {
        long cur = System.currentTimeMillis();
        long elapsed1 = cur - start, elapsed2 = cur - fetch;

        String msg = "finishing the service.."+((double)elapsed1/1000)+" sec elapsed from beginning. fetch: "+
                ((double)elapsed2/1000)+" sec. page title:"+title;
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        Log.i("MyService", msg);
    }

    private String getTextFile(String filepath){
        AssetManager assetManager = getResources().getAssets();
        InputStream is = null;
        BufferedReader br = null;
        String readString = "";
        try {
            try {
                // assetsフォルダ内の sample.txt をオープンする
                is = assetManager.open(filepath);
                br = new BufferedReader(new InputStreamReader(is));

                // １行ずつ読み込み、改行を付加する
                String str;
                while ((str = br.readLine()) != null) {
                    readString += str + "\n";
                }
            } finally {
                if (is != null) is.close();
                if (br != null) br.close();
            }
        } catch (Exception e){
            readString = null;
            // エラー発生時の処理
        }
        return readString;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("MyService", "starting the service");
        Toast.makeText(this, "starting the service", Toast.LENGTH_SHORT).show();
        start = System.currentTimeMillis();

        // このスレッドのハンドラにWebViewの処理をポスト (他スレッドではうまくいかない)
        new Handler().post(new Runnable() {public void run() {
            myWebView = new WebView(MyService.this);

            // キャッシュを無効に (重要, デフォルトのままだとキャッシュのせいで？ダウンロードが発生しない)
            myWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);

            // 画像のロードを無効に / JavaScriptを無効に / ウィンドウを不可視に
            myWebView.getSettings().setLoadsImagesAutomatically(false);
            myWebView.getSettings().setJavaScriptEnabled(true);
            myWebView.getSettings().setUserAgentString("Mozilla/5.0 (Linux; U; Android 4.0.3; ja-jp; SC-02C Build/IML74K) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30");
            myWebView.setVisibility(View.INVISIBLE);

            // demoという変数で DemoJavaScriptInterfaceへのアクセスを可能にする
            myWebView.addJavascriptInterface(new DemoJavaScriptInterface(), "demo");

            // ページロードの終了時に実行するアクションをWebViewClientとともに登録
            myWebView.setWebViewClient(new WebViewClient(){
                @Override
                public void onPageFinished(WebView view, String url) {
                    // Debug.stopMethodTracing();
                    // ページのロードを終了

                    Log.i("MyService", "pageload finished");

                    // JavaScript ソースを読みだす
                    String src = getTextFile("js/test.js");
                    //Log.i("MyService", "src is :" + src);

                    // JavaScriptを実行する
                    myWebView.getSettings().setJavaScriptEnabled(true);
                    //myWebView.loadUrl("javascript:demo.callback(\"\" + document.documentElement.innerHTML);");
                    myWebView.loadUrl("javascript:" + src);
                    Log.i("MyService", "javascript execution issued.");
                }
            });

            // ページをロード
            fetch = System.currentTimeMillis();
            // チケットぴあの野球総合ページへ
            myWebView.loadUrl("http://t.pia.jp/sports/baseball/");
        }});
        return START_STICKY;
    }

    // JavaScript側からコールバックされるオブジェクト
    final class DemoJavaScriptInterface {
        private final int SEQ_INIT = 0;
        private final int SEQ_BASEBALL = 1;
        private final int SEQ_CENTRAL = 2;
        private final int SEQ_YOYAKU = 3;
        private final int SEQ_FINAL = 4;

        private int seq_id;
        DemoJavaScriptInterface() {
            seq_id = SEQ_INIT;
        }
        @JavascriptInterface
        public void callback(String url) {
            Log.i("MyService", "hello!" + url);
            if(seq_id == SEQ_INIT){
                if(!url.equals("")){
                    seq_id = SEQ_FINAL;
                    myWebView.loadUrl(url);
                } else {
                    seq_id = SEQ_FINAL;
                }
            }

            // シーケンス終了
            if(seq_id == SEQ_FINAL){
                stopSelf(); // サービスを止める
            }
        }
    }
}