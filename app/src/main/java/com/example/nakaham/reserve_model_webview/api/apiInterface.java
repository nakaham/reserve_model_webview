package com.example.nakaham.reserve_model_webview.api;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.example.nakaham.reserve_model_webview.api.hooker.impl.apiImplProcess;
import com.example.nakaham.reserve_model_webview.api.hooker.impl.apiInterfaceImpl;
import com.example.nakaham.reserve_model_webview.api.hooker.impl.taskParam;

public class apiInterface {
	protected static int seqNoMaster = 0;
	private apiInterfaceImpl impl;
	Context parent;
	public apiInterface(Context context) {
		parent = context;
	}

	// APIを送信する
	// param:ユーザパラメータ
	// callback:コールバック対象のオブジェクト
	// processImpl:実際のバックグラウンド処理が書かれている apiImplProcess を実装したInstance
	public int sendAPI(	apiParam param,
							final apiReturnCallbackFunction callback,
							final apiImplProcess processImpl,
							String title,
							String message) {
		// AsyncTask は使い捨てにするらしい
		// apiInterfaceImplは AsyncTaskのサブクラスなので
		// やっぱり使い捨てにする必要がある。
		// http://d.hatena.ne.jp/tomorrowkey/20091007/1254915143
		impl = new apiInterfaceImpl(parent);

		// シーケンス番号発行
		param.seqNo = ++seqNoMaster;

		// Handlerから呼び出されるイベントを登録
		taskParam implParam = new taskParam();
		implParam.userParam = param;
		implParam.returnEvent = new Handler(new Handler.Callback() {
			@Override
			public boolean handleMessage(Message msg) {
				// nullじゃなかったらreturn callbackを呼ぶ
				if (callback != null) {
					// msg.obj には apiParam が入ってる
					try {
						processImpl.dispatchToUIThread(callback,(apiParam)msg.obj);
					} catch (Exception e) {
						e.printStackTrace();
					}
					return true;
				}
				return false;
			}
		});
		// 非同期処理の必須設定
		impl.setAPIImplInstance(processImpl);		// 実際の処理を行うインスタンスの設定
		impl.setTitle(title);						// プログレスバーのタイトル設定
		impl.setMessage(message);					// プログレスバーのメッセージ設定

        // 非同期処理開始
		impl.execute(implParam);

		// 時間計測のための測定ポイント
		Log.d("TimeStamp","cmd:" + param.id + "  seqNo:" + param.seqNo);

		// シーケンス番号を返す
		return param.seqNo;
	}
}
