package com.example.nakaham.reserve_model_webview.api.hooker.BayTicket.TickePia.Process;

import android.content.Context;
import android.content.res.AssetManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.example.nakaham.reserve_model_webview.api.apiParam;
import com.example.nakaham.reserve_model_webview.api.apiReturnCallbackFunction;
import com.example.nakaham.reserve_model_webview.api.hooker.BayTicket.TickePia.ReturnInterface.iReturnOnBayTicket;
import com.example.nakaham.reserve_model_webview.api.hooker.BayTicket.TickePia.apiParamBayTicket;
import com.example.nakaham.reserve_model_webview.api.hooker.impl.apiImplProcess;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by nakaham on 2017/04/28.
 */

public class searchAndGetLinkBayTicket implements apiImplProcess {
    Context myContext;
    WebView myWebView;
    public String retval;
    private apiParam.errorCode retCode;
    private synchronized apiParam.errorCode getRetCode(){
        return retCode;
    }
    private synchronized void setRetCode(apiParam.errorCode code){
        retCode = code;
    }
    @Override
    public apiParam.errorCode preProcess(apiParam param1) throws Exception {
        apiParamBayTicket myParam = (apiParamBayTicket)param1;
        myWebView = myParam.targetWebView;
        myContext = myParam.myContext;
        setRetCode(apiParam.errorCode.errorNotInitialized);
        return apiParam.errorCode.noError;
    }
    @Override
    public apiParam.errorCode run(apiParam param1) throws Exception {
        apiParamBayTicket myParam = (apiParamBayTicket)param1;
        searchAndGetLinkBayTicket(myParam.keyword, myParam.url, myParam.type);
        return apiParam.errorCode.noError;
    }

    @Override
    public apiParam.errorCode postProcess(apiParam param1) throws Exception {
        while(getRetCode() == apiParam.errorCode.errorNotInitialized){
            // 成功するか失敗するかするまで待つ
        }
        Log.i("searchAndGetLink", "Process completed at " + ((getRetCode() ==apiParam.errorCode.noError)?"OK":"NG"));
        return apiParam.errorCode.noError;
    }

    @Override
    public apiParam.errorCode dispatchToUIThread(apiReturnCallbackFunction callback, apiParam param1) throws Exception {
        iReturnOnBayTicket myCallback = (iReturnOnBayTicket)callback;
        param1.result = getRetCode();
        myCallback.onCompleteSearchAndGetLinkBayTicket((apiParamBayTicket)param1, null);
        return apiParam.errorCode.noError;
    }

    private String getTextFile(String filepath){
        AssetManager assetManager = myContext.getResources().getAssets();
        InputStream is = null;
        BufferedReader br = null;
        String readString = "";
        try {
            try {
                // assetsフォルダ内の sample.txt をオープンする
                is = assetManager.open(filepath);
                br = new BufferedReader(new InputStreamReader(is));

                // １行ずつ読み込み、改行を付加する
                String str;
                while ((str = br.readLine()) != null) {
                    readString += str + "\n";
                }
            } finally {
                if (is != null) is.close();
                if (br != null) br.close();
            }
        } catch (Exception e){
            readString = null;
            // エラー発生時の処理
        }
        return readString;
    }

    private void searchAndGetLinkBayTicket(final String keyword, final String url, final apiParamBayTicket.pageType ptype){
        Log.i("searchAndGetLink", "starting the process");
        AppCompatActivity activityImstance = (AppCompatActivity)myContext;
        // このスレッドのハンドラにWebViewの処理をポスト (他スレッドではうまくいかない)
        activityImstance.runOnUiThread(new Runnable() {public void run() {
            // キャッシュを無効に (重要, デフォルトのままだとキャッシュのせいで？ダウンロードが発生しない)
            myWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);

            // 画像のロードを無効に / JavaScriptを無効に / ウィンドウを不可視に
            myWebView.getSettings().setLoadsImagesAutomatically(false);
            myWebView.getSettings().setJavaScriptEnabled(true);
            // UserAgentはお好みで
            //myWebView.getSettings().setUserAgentString("Mozilla/5.0 (Linux; U; Android 4.0.3; ja-jp; SC-02C Build/IML74K) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30");
            myWebView.getSettings().setUserAgentString("Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36");
            myWebView.setVisibility(View.INVISIBLE);

            // demoという変数で DemoJavaScriptInterfaceへのアクセスを可能にする
            myWebView.addJavascriptInterface(new myJavaScriptInterface(), "android");

            // ページロードの終了時に実行するアクションをWebViewClientとともに登録
            myWebView.setWebViewClient(new WebViewClient(){
                @Override
                public void onPageFinished(WebView view, String url) {
                    // Debug.stopMethodTracing();
                    // ページのロードを終了
                    Log.i("setWebViewClient", "pageload finished");

                    // JavaScript ソースを読みだす
                    String src = getTextFile("js/test2.js");

                    // ページタイプごとに呼び出すJavaScript API を変える
                    String methodName = "getSrc";

                    // JavaScript ソースに実行行を付与する
                    src += "android.callback(android.returnGetSrc(" + methodName + "()), \"" + keyword + "\")";
                    Log.i("setWebViewClient", "methodName is :" + methodName);

                    // JavaScriptを実行する
                    myWebView.getSettings().setJavaScriptEnabled(true);
                    myWebView.loadUrl("javascript:" + src);
                    Log.i("MyService", "javascript execution issued.");
                }
            });
            // ページジャンプ
            myWebView.loadUrl(url);
        }});
    }

    // searchAndGetLinkTicketPiaBaseBall実行後 JavaScript側からコールバックされるオブジェクト
    final class myJavaScriptInterface {
        myJavaScriptInterface() {
        }

        @JavascriptInterface
        public void callback(String retVal, String keyword) {
            Log.i("MyService", "hello!" + retVal);
            if(!retVal.equals("")){
                // ９回裏 YASUAKI登板
                // HTMLをパース開始
                getLink(keyword);
                searchAndGetLinkBayTicket.this.retval = retVal;
                searchAndGetLinkBayTicket.this.setRetCode(apiParam.errorCode.noError);
            } else {
                // ９回裏 HIRATA登板
                searchAndGetLinkBayTicket.this.retval = "";
                searchAndGetLinkBayTicket.this.setRetCode(apiParam.errorCode.errorGeneral);
            }
        }
        // 取得した動的変化後のソースコードからリンクURLを取得する
        private String getLink(String src){
            String ret = "";
            Document document = Jsoup.parse(retSrc);
            // classでparse
            Elements zaiko_list = document.getElementsByTag("td");
            for(Element list : zaiko_list){
                if(list.text().equals("○")){

                } else if (list.text().equals("△")){

                }
            }
            return "";
        }
        private String retSrc = "";
        @JavascriptInterface
        public void returnGetSrc(String returnValue) {
            retSrc = returnValue;
            Log.d("getMessage関数の戻り値", returnValue);
        }
    }
}
