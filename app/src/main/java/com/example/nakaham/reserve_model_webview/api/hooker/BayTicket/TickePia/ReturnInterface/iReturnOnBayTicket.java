package com.example.nakaham.reserve_model_webview.api.hooker.BayTicket.TickePia.ReturnInterface;

import com.example.nakaham.reserve_model_webview.api.hooker.BayTicket.TickePia.apiParamBayTicket;
import com.example.nakaham.reserve_model_webview.api.hooker.BayTicket.TickePia.ticketData;

import java.util.List;

/**
 * Created by nakaham on 2017/04/28.
 */

public interface iReturnOnBayTicket {
        public void onCompleteSearchAndGetLinkBayTicket(apiParamBayTicket param, List<ticketData> list);
}
