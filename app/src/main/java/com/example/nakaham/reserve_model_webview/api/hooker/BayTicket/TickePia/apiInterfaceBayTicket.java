package com.example.nakaham.reserve_model_webview.api.hooker.BayTicket.TickePia;

import android.content.Context;
import android.webkit.WebView;

import com.example.nakaham.reserve_model_webview.api.apiInterface;
import com.example.nakaham.reserve_model_webview.api.apiReturnCallbackFunction;
import com.example.nakaham.reserve_model_webview.api.hooker.BayTicket.TickePia.Process.searchAndGetLinkBayTicket;

/**
 * Created by nakaham on 2017/04/28.
 */

public class apiInterfaceBayTicket extends apiInterface {
    Context myContext;
    public apiInterfaceBayTicket(Context context) {
        super(context);
        myContext = context;
    }

    // スポーツページから特定のキーワードの興業を探す
    public int searchAndGetLinkBayTicket(String keyword, String url, WebView webv, apiParamBayTicket.pageType ptype, apiReturnCallbackFunction callback) {

        apiParamBayTicket param = new apiParamBayTicket();
        param.id = this.getClass().getName();
        param.keyword = keyword;
        param.myContext = myContext;
        param.url = url;
        param.targetWebView = webv;
        param.type = ptype;

        // 非同期コールで日付から株の一覧を取得する
        return sendAPI(param,
                callback,
                new searchAndGetLinkBayTicket(),
                "Please wait",
                "" + keyword + "を検索中...");
    }
}
