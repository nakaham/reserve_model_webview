package com.example.nakaham.reserve_model_webview.api.hooker.BayTicket.TickePia;

import android.content.Context;
import android.webkit.WebView;

import com.example.nakaham.reserve_model_webview.api.apiParam;

/**
 * Created by nakaham on 2017/04/28.
 */

public class apiParamBayTicket extends apiParam {
    public static enum pageType {
        baseballPage,
        crimaxPage;
    }

    public String  keyword;
    public Context myContext;
    public WebView targetWebView;
    public String  url;
    public pageType type;
}