package com.example.nakaham.reserve_model_webview.api.hooker.TickePia.ReturnInterface;

import com.example.nakaham.reserve_model_webview.api.hooker.TickePia.apiParamTicketPia;

/**
 * Created by nakaham on 2017/04/28.
 */

public interface iReturnOnTicketPia {
        public void onCompleteSearchAndGetLinkTicketPiaBaseball(apiParamTicketPia param, String url);
}
