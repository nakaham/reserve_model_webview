package com.example.nakaham.reserve_model_webview.api.hooker.TickePia;

import android.content.Context;
import android.content.res.AssetManager;
import android.webkit.WebView;

import com.example.nakaham.reserve_model_webview.api.apiInterface;
import com.example.nakaham.reserve_model_webview.api.apiReturnCallbackFunction;
import com.example.nakaham.reserve_model_webview.api.hooker.TickePia.Process.searchAndGetLinkTicketPiaBaseBall;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by nakaham on 2017/04/28.
 */

public class apiInterfaceTicketPia extends apiInterface {
    Context myContext;
    public apiInterfaceTicketPia(Context context) {
        super(context);
        myContext = context;
    }

    // スポーツページから特定のキーワードの興業を探す
    public int searchAndGetLinkTicketPiaBaseBall(String keyword, String url, WebView webv, apiParamTicketPia.pageType ptype, apiReturnCallbackFunction callback) {

        apiParamTicketPia param = new apiParamTicketPia();
        param.id = this.getClass().getName();
        param.keyword = keyword;
        param.myContext = myContext;
        param.url = url;
        param.targetWebView = webv;
        param.type = ptype;

        // 非同期コールで日付から株の一覧を取得する
        return sendAPI(param,
                callback,
                new searchAndGetLinkTicketPiaBaseBall(),
                "Please wait",
                "" + keyword + "を検索中...");
    }
}
