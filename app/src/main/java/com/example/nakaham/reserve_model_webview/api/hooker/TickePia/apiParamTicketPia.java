package com.example.nakaham.reserve_model_webview.api.hooker.TickePia;

import android.content.Context;
import android.database.sqlite.SQLiteCursor;
import android.webkit.WebView;

import com.example.nakaham.reserve_model_webview.api.apiParam;

/**
 * Created by nakaham on 2017/04/28.
 */

public class apiParamTicketPia extends apiParam {
    public static enum pageType {
        baseballPage,
        crimaxPage;
    }

    public String  keyword;
    public Context myContext;
    public WebView targetWebView;
    public String  url;
    public pageType type;
}
