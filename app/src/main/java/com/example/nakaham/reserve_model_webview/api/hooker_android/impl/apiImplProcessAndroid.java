package com.example.nakaham.reserve_model_webview.api.hooker_android.impl;

import com.example.nakaham.reserve_model_webview.api.apiParam;
import com.example.nakaham.reserve_model_webview.api.apiReturnCallbackFunction;

public interface apiImplProcessAndroid {
	// 前処理
	public apiParam.errorCode preProcess(apiParam param1) throws Exception ;
	// 本処理
	public apiParam.errorCode run(apiParam param1) throws Exception ;
	// 後処理
	public apiParam.errorCode postProcess(apiParam param1) throws Exception ;
	// UIスレッドへディスパッチし、onほげほげcallbackを行う
	public apiParam.errorCode dispatchToUIThread(apiReturnCallbackFunction callback, apiParam param1) throws Exception ;
}
