package com.example.nakaham.reserve_model_webview.api.hooker_android.impl;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.os.AsyncTask;
import android.os.Message;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class apiInterfaceImplAndroid extends AsyncTask<taskParam, taskParam, taskParam>
		implements OnCancelListener {
	// プログレスダイアログ
	ProgressDialog dialog;
	Context context;

	public apiInterfaceImplAndroid(Context context) {
		this.context = context;
	}
	// 使用するAPIを登録する
	private List<apiImplProcessAndroid> implInstanceList = new ArrayList<apiImplProcessAndroid>();

	public void setAPIImplInstance(apiImplProcessAndroid instance){
		implInstanceList.add(instance);
	}

	// プログレスウインドウのタイトルを設定
	private String _myTitle = "Please wait";
	public void setTitle(String txtTitle){
		_myTitle = txtTitle;
	}

	// プログレスウインドウのタイトルを設定
	private String _myMessage = "Processing ...";
	public void setMessage(String txtMessage){
		_myMessage = txtMessage;
	}

	@Override
	protected void onPreExecute() {
		dialog = new ProgressDialog(context);
		dialog.setTitle(_myTitle);
		dialog.setMessage(_myMessage);
		dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		dialog.setCancelable(true);
		dialog.setOnCancelListener(this);
		dialog.show();
	}

	@Override
	protected taskParam doInBackground(taskParam... param1) {
		if(implInstanceList.size()!=0){
			try {
				implInstanceList.get(0).preProcess(param1[0].userParam);
				param1[0].userParam.result = implInstanceList.get(0).run(param1[0].userParam);
				implInstanceList.get(0).postProcess(param1[0].userParam);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		param1[0].progress = 0;
		param1[0].resultVal = "";

		return param1[0];
	}

	@Override
	protected void onProgressUpdate(taskParam... param2) {
		// dialog.setProgress(values[0]);
	}

	@Override
	protected void onCancelled() {
		dialog.dismiss();
	}

	@Override
	protected void onPostExecute(taskParam result) {
		dialog.dismiss();

		Log.d("onPostExecute", "start");
		// HandlerのCallbackを呼び出す
		Message msg = new Message();
		msg.obj = result.userParam;						// 引数オブジェクトを記載
		
		result.returnEvent.sendMessage(msg);
	}

	@Override
	public void onCancel(DialogInterface dialog) {
		this.cancel(true);
	}
}
