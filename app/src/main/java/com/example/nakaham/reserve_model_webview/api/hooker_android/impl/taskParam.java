package com.example.nakaham.reserve_model_webview.api.hooker_android.impl;

import android.os.Handler;

import com.example.nakaham.reserve_model_webview.api.apiParam;

// TASK内で使用する引数クラス
public class taskParam {
	public apiParam userParam;
	public Integer progress;
	public String resultVal;
	// リターンコールバックイベント
	public Handler returnEvent;
}
