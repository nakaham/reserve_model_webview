package com.example.nakaham.reserve_model_webview;

import java.text.NumberFormat;

/**
 * Created by nakaham on 2017/06/25.
 */

public class commonUtil {
    public static boolean isNumber(String num) {
        try {
            Number number = NumberFormat.getInstance().parse(num);
            int i = number.intValue();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public static int convToNumber(String stockval){
        String sval = stockval.replaceAll(",","");
        int ret = Integer.MAX_VALUE;;
        // 数値じゃない値が入ってたらソート対象外とする
        if(isNumber(sval) == false) return Integer.MAX_VALUE;
        try{
            // intに変換して返すのをtry
            Number number = NumberFormat.getInstance().parse(stockval);
            ret = number.intValue();
        } catch(Exception ex) {
            // 失敗したらソート対象外
            ex.printStackTrace();
            ret = Integer.MAX_VALUE;
        }
        return ret;
    }

}
